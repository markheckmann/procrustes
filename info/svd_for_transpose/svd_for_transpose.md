# SVD for transpose





Let $\mathbf{A}(m \times n)$ and $\mathbf{B}(m \times n)$ be two real-valued matrices. Minimizing $\| \mathbf{AQ} - \mathbf{B} \|^2_F$ with $\mathbf{Q}$ being a rotation matrix is achieved by 

$$\mathbf{Q} = \mathbf{U} \mathbf{V}^T$$

where the SVD of $\mathbf{A}^T\mathbf{B}$ being

$$\mathbf{A}^T\mathbf{B} = \mathbf{UDV}^T \qquad \text{(case 1)}.$$

Generally, I would assume that we get the same results by using the transpose of $\mathbf{A}^T\mathbf{B}$, i.e.

$$\mathbf{B}^T\mathbf{A} = \mathbf{UDV}^T \qquad \text{(case 2)}$$

and calculating 

$$\mathbf{Q} = \mathbf{V} \mathbf{U}^T$$

However, the two solution are different for $m < n$. An example (calculated in R)



$$
 \mathbf{A}= \begin{pmatrix}
 0.0 & -0.5 & -1.0 \\ 
 0.0 & 0.5 & 1.0 \\ 
 \end{pmatrix} \mkern36mu \mathbf{B}= \begin{pmatrix}
 0.5 & 0.5 & 0.0 \\ 
 -0.5 & -0.5 & 0.0 \\ 
 \end{pmatrix} $$
 

When calculating $\mathbf{Q}$ by factoring according to case 1 (subscript 1) we get




$$
 \mathbf{Q_1}= \begin{pmatrix}
 -0.32 & 0.32 & 0.89 \\ 
 -0.88 & 0.25 & -0.40 \\ 
 -0.35 & -0.92 & 0.20 \\ 
 \end{pmatrix} $$
 

For case 2 (subscript 2) we get

$$
 \mathbf{Q_2}= \begin{pmatrix}
 0.00 & 0.00 & 1.00 \\ 
 -0.95 & 0.32 & 0.00 \\ 
 -0.32 & -0.95 & 0.00 \\ 
 \end{pmatrix} $$
 

Both solutions give the same result for the initial problem, as $\mathbf{AQ_1} = \mathbf{AQ_2}$ Yet, the rotations matrices are different.

$$
 \mathbf{AQ_1}= \begin{pmatrix}
 0.79 & 0.79 & -0.00 \\ 
 -0.79 & -0.79 & 0.00 \\ 
 \end{pmatrix} \mkern72mu \mathbf{AQ_2}= \begin{pmatrix}
 0.79 & 0.79 & 0.00 \\ 
 -0.79 & -0.79 & 0.00 \\ 
 \end{pmatrix} $$
 

I do not really understand why this is the case and how the results relate to another. Can someone explain this in a way, so it can be understood by a non-mathematician like me? ;)


