

%\VignetteEngine{knitr::knitr}
%\VignetteIndexEntry{Use mat2tex with knitr and .Rnw files}

\documentclass[article,nojss]{jss}\usepackage[]{graphicx}\usepackage[]{color}
%% maxwidth is the original width if it is less than linewidth
%% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\definecolor{fgcolor}{rgb}{0.345, 0.345, 0.345}
\newcommand{\hlnum}[1]{\textcolor[rgb]{0.686,0.059,0.569}{#1}}%
\newcommand{\hlstr}[1]{\textcolor[rgb]{0.192,0.494,0.8}{#1}}%
\newcommand{\hlcom}[1]{\textcolor[rgb]{0.678,0.584,0.686}{\textit{#1}}}%
\newcommand{\hlopt}[1]{\textcolor[rgb]{0,0,0}{#1}}%
\newcommand{\hlstd}[1]{\textcolor[rgb]{0.345,0.345,0.345}{#1}}%
\newcommand{\hlkwa}[1]{\textcolor[rgb]{0.161,0.373,0.58}{\textbf{#1}}}%
\newcommand{\hlkwb}[1]{\textcolor[rgb]{0.69,0.353,0.396}{#1}}%
\newcommand{\hlkwc}[1]{\textcolor[rgb]{0.333,0.667,0.333}{#1}}%
\newcommand{\hlkwd}[1]{\textcolor[rgb]{0.737,0.353,0.396}{\textbf{#1}}}%

\usepackage{framed}
\makeatletter
\newenvironment{kframe}{%
 \def\at@end@of@kframe{}%
 \ifinner\ifhmode%
  \def\at@end@of@kframe{\end{minipage}}%
  \begin{minipage}{\columnwidth}%
 \fi\fi%
 \def\FrameCommand##1{\hskip\@totalleftmargin \hskip-\fboxsep
 \colorbox{shadecolor}{##1}\hskip-\fboxsep
     % There is no \\@totalrightmargin, so:
     \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
 \MakeFramed {\advance\hsize-\width
   \@totalleftmargin\z@ \linewidth\hsize
   \@setminipage}}%
 {\par\unskip\endMakeFramed%
 \at@end@of@kframe}
\makeatother

\definecolor{shadecolor}{rgb}{.97, .97, .97}
\definecolor{messagecolor}{rgb}{0, 0, 0}
\definecolor{warningcolor}{rgb}{1, 0, 1}
\definecolor{errorcolor}{rgb}{1, 0, 0}
\newenvironment{knitrout}{}{} % an empty environment to be redefined in TeX

\usepackage{alltt}

\usepackage{amsmath}
\usepackage{color}
\usepackage{relsize}  % for mathsmaller
\usepackage{pbox}
\usepackage[utf8]{inputenc}

%\usepackage[urlcolor=blue, colorlinks=true, pdfborder={0 0 0}]{hyperref}

% \title{\texttt{mat2tex} R package \linebreak \linebreak 
%         \normalsize{Version 0.1} \linebreak \linebreak 
%         \url{https://github.com/markheckmann/mat2tex}}
% \author{Mark Heckmann}
% \date{\today}

\parindent0mm


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\newcommand{\di}{\textbf{\textsf{diagram}}\xspace}
\newcommand{\T}{{ ^{\mathsmaller T} }}
\newcommand{\B}[1] { \mathbf{#1} }  % bold math: to make it easy to change from \mathbf to \bm etc.
\newcommand{\MN}[1]{ \marginnote{\footnotesize \textcolor{red}{#1} } }  % für korrekturfahnen

\title{\proglang{R} package \pkg{procrustes}: Several algorithms from the procrustes family of transformations}

\Plaintitle{R package procrustes: Several algorithms from the procrustes family of transformations}

\Keywords{procrustes, configurations, fitting, R}

\Plainkeywords{procrustes, configurations, fitting, R}


\author{Mark Heckmann\\
University of Bremen, Germany \\
\normalsize{Version 0.1} \\ 
\url{https://github.com/markheckmann/procrustes}
}

\Plainauthor{Mark Heckmann}

\Abstract{This document describes several features of the \pkg{procrustes} package.
The procrustes package was written to facilitate my PhD work which includes several types of procrustes transformations.
The available package lacked some features I needed, so I implemented them myself.
}

%% The address of (at least) one author should be given
%% in the following format:
\Address{
  Mark Heckmann\\
  University of Bremen, Germany\\
  E-mail: \email{heckmann@uni-bremen.de}\\
  R-blog: \url{http://ryouready.wordpress.com}\\
  Website: \url{http://www.markheckmann.de}\\
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\begin{document}
 
\maketitle






\section{Quick start}

To use the \verb+procrustes+ package, install 

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{library}\hlstd{(devtools)}
\hlkwd{install_github}\hlstd{(}\hlstr{"markheckmann/procrustes"}\hlstd{)}
\end{alltt}
\end{kframe}
\end{knitrout}

and load it.

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{library}\hlstd{(procrustes)}
\end{alltt}
\end{kframe}
\end{knitrout}



\section{Orthogonal procrustes analysis}

Mathematically a configuration is represented by a $m \times n$ matrix, where each row represents a point and each column a dimension. Let the matrix $\B{A}$ represent a small configuration with three points in $\mathbf{R}^2$. 

$$
 \B{A} =  \begin{pmatrix}
 4.0 & 0.0 \\ 
 -4.8 & 1.4 \\ 
 0.7 & -5.0 \\ 
 \end{pmatrix} \mkern36mu \B{B} =  \begin{pmatrix}
 9.5 & 2.1 \\ 
 -4.9 & 7.7 \\ 
 -4.5 & -9.7 \\ 
 \end{pmatrix} $$
 


The standard orthogonal procrustes analysis (OPA) has the goal to fit a configuration $\B{A}$ to a another configuration $\B{B}$ as closely as possible by scaling, rotating and reflecting, and translating configuration $\B{A}$. 

\begin{equation} \label{eq:opa-formulation}
  \B{B} = \underset{ \text{scaling} }{c} 
           \B{A} \underset{ \substack{\text{rotation} + \\ \text{reflection}} }{ \B{Q} } + 
           \underset{ \text{translation}}{ J\gamma\T } +
           \underset{ \text{error}}{ \B{E} }
\end{equation}

Closeness is measured as the sum of squared distances between the row vectors (homologous points) of the two configurations. So the goal is to minimize the function

\begin{equation} 
f(c, \B{Q}, \gamma) = tr\; \mathbf{E\T E} 
\end{equation}

with respect to the transformations that are allowed. The function \texttt{opa} performs this type of analysis. \texttt{opa} also allows to apply all combinations of the possible transformations.\footnote{One of the main reasons to write this package was that I needed a function where I can do this.} I.e. all three transformations given in \eqref{eq:opa-formulation} can be switched on or off seperately. The solution to these minimization-problems are special cases of the generalized solution to the orthogonal procrustes problem as outlined by \cite{schonemann_fitting_1970}. 


\section{Models}

\subsection{Scaling-only}

This is a very unsual case, as it only makes sense if the configurations already have a natural center. This might be useful for free choice profiling data where the midpoint is used for centering. The formulation is

\begin{equation} 
  \B{B} = \underset{ \text{scaling} }{c}  \B{A} + \B{E} 
\end{equation}

Figure \ref{fig:opa-S} shows the centered matrices $\B{A}$ and $\B{B}$ before and after the scaling only procrustes.

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{opa}\hlstd{(A, B,} \hlkwc{model}\hlstd{=}\hlstr{"S"}\hlstd{)}
\end{alltt}
\end{kframe}
\end{knitrout}


\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}[]

\includegraphics[width=\maxwidth]{figure/opa-S} \caption[Isotropic scaling only]{Isotropic scaling only. Configurations (a) before (b) after scaling.\label{fig:opa-S}}
\end{figure}


\end{knitrout}


\subsection{Rotation only}

The solution to this problem for the general case can e.g. be found in \cite{schonemann_generalized_1966}.

\begin{equation} 
  \B{B} =  \B{A} \underset{ \substack{\text{rotation} + \\ \text{reflection}} }{ \B{Q} } 
           \underset{ \text{error}}{ \B{E} }
\end{equation}

Figure \ref{fig:opa-R} shows the configurations before and after a rotation only transformation.

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{opa}\hlstd{(A, B,} \hlkwc{model}\hlstd{=}\hlstr{"R"}\hlstd{)}
\end{alltt}
\end{kframe}
\end{knitrout}


\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}[]

\includegraphics[width=\maxwidth]{figure/opa-R} \caption[Orthogonal rotation only]{Orthogonal rotation only. Configurations (a) before (b) after scaling.\label{fig:opa-R}}
\end{figure}


\end{knitrout}



\subsection{Scaling and rotation}

This is a typical case for naturally centered data where the row points will be rotated orthogonally and fitted in length.

\begin{equation} 
  \B{B} = \underset{ \text{scaling} }{c} 
           \B{A} \underset{ \substack{\text{rotation} + \\ \text{reflection}} }{ \B{Q} } + 
           \underset{ \text{error}}{ \B{E} }
\end{equation}

Figure \ref{fig:opa-SR} shows the configurations before and after rotation and sclaing.

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{opa}\hlstd{(A, B,} \hlkwc{model}\hlstd{=}\hlstr{"SR"}\hlstd{)}
\end{alltt}
\end{kframe}
\end{knitrout}


\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}[]

\includegraphics[width=\maxwidth]{figure/opa-SR} \caption[Scaling and rotation]{Scaling and rotation. Configurations (a) before (b) after transformation.\label{fig:opa-SR}}
\end{figure}


\end{knitrout}


\subsection{Scaling, rotation and translation}

The following code gives the transformation with all transformations. The results are shown in Figure \ref{fig:opa-SRT}.

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlkwd{opa}\hlstd{(A, B)}
\end{alltt}
\end{kframe}
\end{knitrout}


\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{figure}[]

\includegraphics[width=\maxwidth]{figure/opa-SRT} \caption[Scaling and rotation]{Scaling and rotation. Configurations (a) before (b) after transformation.\label{fig:opa-SRT}}
\end{figure}


\end{knitrout}





\section{Appendix}


\section{Basic transformations}


\subsection{Translation}

\subsection{Appendix}

The solutions to all subsets of parameters of the simple procrustes problem are listed below.
They can easily be derived along the same lines as in \cite{schonemann_fitting_1970}

\begin{tabular}{llll}
\hline
Model & \multicolumn{3}{c}{Parameters} \\
\hline
 & $\B{Q}$ & $c$  & $\gamma$ \\
\cline{2-4}
% S
 $f(c)$ & 
   - & 
   $\frac{ tr\;\B{A}\T \B{B} }{ tr\; \B{A}\T \B{A} }$ & 
   - \\[10pt]
% R   
 $f(\B{Q})$ & 
   \pbox{20cm}{$\B{Q} = \B{UV}\T$ with \\ $\B{UDV}\T = \B{A}\T\B{B}$} & 
   -  & 
   - \\[10pt]
% T
  $f(\gamma)$ & 
    - & 
    -  & 
    $(\B{B} - \B{A})\T \B{J} / p$ \\[10pt]
% SR    
  $f(c,\B{Q})$ & 
    \pbox{20cm}{$\B{Q} = \B{UV}\T$ with \\ $\B{UDV}\T = \B{A}\T\B{B}$} & 
    $\frac{ tr\; \B{Q}\T \B{A}\T \B{B} }{ tr\; \B{A}\T \B{A} }$ & 
    - \\[10pt]
% ST    
  $f(c, \gamma)$ & 
    - & 
    $\frac{ tr\; \B{A}\T (\B{B} - \B{J}\gamma\T ) }{ tr\; \B{A}\T \B{A} }$ & 
    $(\B{B} - \B{A})\T \B{J} / p$ \\[10pt]  
% RT
  $f(\B{Q}, \gamma)$ & 
    \pbox{20cm}{$\B{Q} = \B{UV}\T$ with \\ $\B{UDV}\T = \B{A}\T(\B{I} - \B{JJ}\T / p)\B{B}$} & 
    - & 
    $(\B{B} - \B{AQ})\T \B{J} / p$ \\[10pt] 
% SRT (full model)
  $f(c, \B{Q}, \gamma)$   & 
    \pbox{20cm}{$\B{Q} = \B{UV}\T$ with \\ $\B{UDV}\T = \B{A}\T(\B{I} - \B{JJ}\T / p)\B{B}$} & 
    $\frac{ tr\;\B{Q}\T \B{A}\T (\B{I} - \B{JJ}\T / p) \B{B} }{ tr\; \B{A}\T (\B{I} - \B{JJ}\T / p) \B{A} }$ & 
    $(\B{B} - c\B{AQ})\T \B{J} / p$ \\[10pt]  
\hline
\end{tabular}




%\bibliographystyle{plain}
\bibliography{literature}


\end{document}
