library(procrustes)
library(testthat)

# TODO :(
context("OPA - against other packages")

if (require(shapes)) {
  
  test_that("results from opa and shapes::procOPA are identical", {
    
  # comparison only works when target is centered as procOPA always uses centering
  # The test works basically. But procOPA uses another factoring than OPA.
  # Espeically for m > n there are situtaions where the (see info/svd_for_transpose)
  # the resut are the same but the rotation matrices are not m > n (see info/svd_for_transpose).
  # Also there may be situtaions which are have to solutions not only one
  # (see info/ope_two_solutions). That is why here onyl the SSQ are compared which always should
  # be identical.
      
    one_comparison <- function()
    {
      p <- sample(2:10, 1)
      n <- sample(2:10, 1)
      rot <- random_matrix(n, p, digits=0)
      rot <- scale(rot, scale=F)
      target <- random_matrix(n, p, digits=0)
      target <- scale(target, scale=F)  
      
      g1 <- shapes::procOPA(target, rot, scale = FALSE, reflect = TRUE)
      g2 <- opa(rot, target, model="RT")

      rss1 <- rss(g1$Bhat, target)
      rss2 <- rss(g2$Yhat, target)
      all.equal(rss1, rss2)
         
      # The following may fail (see above)
      #f <- all(c(all.equal(g1$Bhat, g2$Yhat)))#,   # same result for Bhat?
            #all.equal(g1$R, g2$Q)) )               # same rotation matrices
#       if (is.na(f))
#         browser()
    }   
    
    res <- replicate(1000, one_comparison())
    expect_true(all(res))
  }) 
}


