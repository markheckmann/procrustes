library(procrustes)
library(testthat)

context("GPA - proc_gower_75")


test_that("proc_gower_75 results do not change when input is rotated randomly", {
  
  compare_ssq <- function()
  {
    p <- sample(2:10, 1)
    n <- sample(2:10, 1)
    m <- sample(3:20, 1)
    x <- random_matrix_list(m, n, p)
    
    g <- proc_gower_75(x, center = T) 
    r <- rotate_randomly(x)
    
    g2 <- proc_gower_75(r, center = T)   
    all.equal(g$ssq.res, g2$ssq.res)
  }

  res <- replicate(10, compare_ssq())
  expect_true(all(res))
})



test_that("proc_gower_75 results are identical if rotations matrices are input", {
  
  compare_results <- function()
  {
    p <- sample(2:10, 1)
    m <- sample(3:20, 1)
    qq <- random_rotation_matrices(m, p)      # random rotation matrices
    center <- sample(c(T,F), 1)
    scaling <- sample(c(T,F), 1)
    fixvariation <- sample(c(T,F), 1)
    g <- proc_gower_75(qq, center = center, 
                       scaling = scaling, 
                       fixvariation = fixvariation)  
    all_list_entries_equal(g$Xrot)            # all matrices should be the same 
  }
  
  res <- replicate(100, compare_results())
  expect_true(all(res))
})





