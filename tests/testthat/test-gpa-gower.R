library(procrustes)
library(testthat)

context("GPA - Gower's algorithm")

test_that("GPA algo always decreses criterion", {
  
  random_gpa <- function() 
  {
    seed <- sample(1:2000, 1)
    set.seed(seed)
    p <- sample(2:10, 1)
    n <- sample(2:10, 1)
    m <- sample(3:20, 1)
    x <- random_matrix_list(m, n, p)
    r <- gpa_gower(x)
    monotonic(r$gg, increasing = FALSE)
  }
  res <- replicate(10, random_gpa())
  expect_true(all(res))
})



test_that("SSQ does not change when input is rotated randomly", {
  
  compare_ssq <- function()
  {
    p <- sample(2:10, 1)
    n <- sample(2:10, 1)
    m <- sample(3:20, 1)
    x <- random_matrix_list(m, n, p)
    
    g <- gpa_gower(x, center = T) 
    r <- rotate_randomly(x)
    
    g2 <- gpa_gower(r, center = T)   
    all.equal(g$ssq$ssq.residual, g2$ssq$ssq.residual)
  }

  res <- replicate(10, compare_ssq())
  expect_true(all(res))
})





