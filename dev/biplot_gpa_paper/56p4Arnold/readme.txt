 Biplots of free-choice profile data in generalized orthogonal Procrustes analysis
G. M. Arnold, J. C. Gower, S. Gardner-Lubbe and N. le Roux
Appl. Statist., 56 (2007), 445 - 458

Description of data set
The port wine data set is described in section 3 of the paper and is given in Table 1. The statistical analysis described in the paper was conducted using the open source package R obtainable from http://www.r-project.org/ .  The data set as used by the R functions is available as the dataframe FCPport.repl1.data.

R functions for conducting the statistical analysis described in the paper.
The following  R functions were written to perform the statistical analysis described in the paper:
FCP.biplot.JRSS, drawbipl.streep, Draw.mmline and Draw.Fig.4. These functions together with the dataframe FCPport.repl1.data are to be sourced after initialising an R session. The functions were written to produce the results described in the paper but they can easily be adapted for analysing a user�s own data.  

The function FCP.biplot.JRSS is the main function for performing the generalised orthogonal Procrustes analysis of free-choice-profile data.  This function calls the functions drawbipl.streep and Draw.mmline for constructing biplots.  

Arguments of function FCP.biplot.JRSS:
	data = dataframe or matrix containing the data (default is FCPport.repl1.data).
	isotropic = T or F. Determines if isotropic scaling is to be used in the orthogonal Procrustes 		algorithm.
	Pk.scaling = T or F. Determines if Pk-scaling as described in section 3 of the paper is to be 		used or the alternative scaling described in section 4.
 	eps = 1e-07. Convergence criterion for the orthogonal Procrustes algorithm.
 	iter = 10. Maximum number of iterations allowed for the orthogonal Procrustes algorithm.	colours = vector of colours used for colouring biplot axes.
n.int =  number of scale markers used on biplot axes. Default is 5 and might need to be  
 	increased for constructing biplots.
...  = argument for passing additional parameters to function drawbipl.streep for controlling 
 	appearance of biplots.

The following function calls are used for producing the output described in the paper: 

The call FCP.biplot.JRSS(Pk.scaling=T, isotropic=T, iter=20) produces the generalised orthogonal Procrustes analysis output described in the paper together with Figures 2(a), 3 and 6(a) as well as the contents of Table 2 and the sk (Pk-scaling) values given in Table 3. 

Figure 6(b) can be obtained using the call FCP.biplot.JRSS(Pk.scaling=F, isotropic=F, iter=20, n.int=10) while the sk-values in the second column of Table 3 are obtained using the call FCP.biplot.JRSS(Pk.scaling=F, isotropic=T, iter=20, n.int=10).

Draw.Fig.4(iter=20) produces Figure 4 of the paper.

Notice that the actual biplots resulting from the above calls might appear in rotated form. However, this does not make any difference to the results.
 
The above functions are maintained by NJ le Roux (email: njlr@sun.ac.za) and Sugnet Gardner-Lubbe (email: Sugnet_Lubbe@BAT.com ).


J. C. Gower
Department of Statistics
Walton Hall
The Open University
Milton Keynes
MK7 6AA
UK

E-mail: j.c.gower@open.ac.uk