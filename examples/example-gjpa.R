# examples for generalized joined procrustes analysis (GJPA)

library(scales)


#### simple example ####

set.seed(2)
x <- random_matrix_list(5, n=3, p=2, digits = 1)
y <- random_classification_list(5, k=2, p=2)
r <- gjpa(x, y, w=1)
xq <- r$xq
yq <- r$yq
viz_configs(xq[[1]], xq[[2]], yq[[1]], yq[[2]])

# plot_setup(r$xq)
# add_points(r$xq)
# add_labels(r$xq)
# add_stars(r$xq, col=alpha(1, .3), lwd=3)
# add_attributes(r$yq)



#### GJPA with different balancing factors ####

# GJPA for two matrices (JPA case)

A1 <- matrix(c(1,1, 
               0,-1, 
               -1,1), ncol=2, byrow=T) * 1.2
A2 <- diag(2)
Q <- rotation_matrix_2d(pi/4)
B1 <- matrix(c(1, .8, 
               .2, -1, 
               -1.2,1), ncol=2, byrow=T) *1.2
B1 <- B1 %*% Q 
B2 <- diag(2)

x <- list(A1, B1)
y <- list(A2, B2)
op <- par(mfrow=c(2,2), mar=c(0,0,1,0))
ww <- seq(1,0, length=2*2)
for (w in ww) {
  r <- gjpa(x, y, w=w, center = FALSE)
  xq <- r$xq
  yq <- r$yq
  viz_configs(xq[[1]], xq[[2]], yq[[1]], yq[[2]])
  lbl <- paste("w=", round(w, 2))
  ptext(lbl)
}
par(op)


#### More than 2 matrices ####

set.seed(4)  
n = 3
p = 2
m = 3     # number of matrices
k = 3     # number of themes/categories

x <- random_matrix_list(m, n, p)
y <- replicate(m, diag(2), simplify = FALSE)

op <- par(mfrow=c(2,2), mar=c(0,0,1,0))
ww <- seq(1,0, length=2*2)
for (w in ww) {
  r <- gjpa(x, y, w=w, center = TRUE)
  
  plot_setup(r$xq)
  add_points(r$xq)
  add_labels(r$xq)
  add_stars(r$xq, col=alpha(1, .3), lwd=3)
  add_attributes(r$yq)
  lbl <- paste("w=", round(w, 2))
  ptext(lbl)
}
par(op)



#### More than 2 matrices ####

set.seed(4)  
n = 3
p = 2
m = 9     # number of matrices
k = 3     # number of themes/categories

x <- random_matrix_list(m, n, p)
y <- replicate(m, diag(2), simplify = FALSE)

op <- par(mfrow=c(3,3), mar=c(0,0,1,0))
ww <- seq(.48,.42, length=3*3)
for (w in ww) {
  r <- gjpa(x, y, w=w, center = TRUE)
  
  plot_setup(r$xq)
  add_points(r$xq)
  add_labels(r$xq)
  add_stars(r$xq, col=alpha(1, .3), lwd=3)
  add_attributes(r$yq, col="blue")
  
  lbl <- paste("w=", round(w, 2))
  ptext(lbl)
}
par(op)












