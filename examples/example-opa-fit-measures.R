# Example from Lingoes and Schönemann (1974, p.427)
# using data from Schönemann and Caroll (1970, p.250, TABLE 2)

A2 = matrix(c(4, 0, 
              3, 1.414,
              0, 1.414,
              0, 0), by=T, ncol=2)              
B2 = matrix(c(4.456, 1.688,
              .165, 1.646,
              -3.230,-2.045,
              -1.114, -4.235), by=T, ncol=2)
p <- opa(A2, B2)
f <- fit(p)
f
f$S^.5   # to replicate value in Lingoes & Schönemann (1974, p.427)
